FROM centos:7

RUN yum update -y && yum install -y wget make gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel
RUN cd /usr/src && \
	wget https://www.python.org/ftp/python/3.7.12/Python-3.7.12.tgz && \
	tar xzf Python-3.7.12.tgz && \
	cd Python-3.7.12 && \
	./configure --enable-optimizations && \
	make install; \
	rm /usr/src/Python-3.7.12.tgz 
    
RUN pip3 install flask flask-jsonpify flask-restful

RUN mkdir /python_api
COPY python-api.py /python_api/
WORKDIR /python_api

EXPOSE 5290
CMD ["python3.7", "python-api.py"]

